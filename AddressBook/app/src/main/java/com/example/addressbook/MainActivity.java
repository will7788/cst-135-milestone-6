package com.example.addressbook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button add, remove, edit, display, sort, search;
    public static final String bookpass = "com.example.addressbook.bookpass";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        add = (Button) findViewById(R.id.button);
        remove = (Button) findViewById(R.id.button2);
        edit = (Button) findViewById(R.id.button3);
        display = (Button) findViewById(R.id.button4);
        sort = (Button) findViewById(R.id.button5);
        search = (Button) findViewById(R.id.button6);

        add.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                OpenAdd();
            }
        });

        remove.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                OpenRemove();
            }
        });

        edit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                OpenEdit();
            }
        });

        display.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                OpenDisplay();
            }
        });

        sort.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                OpenSort();
            }
        });

        search.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                OpenSearch();
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();

    }

    public void OpenAdd()
    {
        Intent intent = new Intent(this, AddContact.class);
        startActivity(intent);
    }
    public void OpenRemove()
    {
        Intent intent = new Intent(this, RemoveContact.class);
        startActivity(intent);
    }
    public void OpenEdit()
    {
        Intent intent = new Intent(this, EditContact.class);
        startActivity(intent);
    }
    public void OpenDisplay()
    {
        Intent intent = new Intent(this, DisplayContacts.class);
        startActivity(intent);
    }
    public void OpenSort()
    {
        Intent intent = new Intent(this, SortContacts.class);
        startActivity(intent);
    }

    public void OpenSearch()
    {
        Intent intent = new Intent(this, SearchContacts.class);
        startActivity(intent);
    }

}
