package com.example.addressbook;

public class Photo {
    private int id;
    private String filename;
    private String date;
    private String description;

    public Photo() {

    }

    public Photo(int id, String filename, String date, String description) {
        this.id = id;
        this.filename = filename;
        this.date = date;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Photo [id=" + id + ", filename=" + filename + ", date=" + date + ", description=" + description + "]";
    }
}
