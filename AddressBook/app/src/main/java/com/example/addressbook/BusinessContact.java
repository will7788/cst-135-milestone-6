package com.example.addressbook;
import java.util.ArrayList;

public class BusinessContact extends BaseContact {

    private String operatinghours;
    private String url;

    public BusinessContact()
    {

    }

    public BusinessContact(int number, String name, int phone, ArrayList<Photo> photos, Location location, String operatinghours, String url)
    {
        super(number, name, phone, photos, location);
        this.setOperatinghours(operatinghours);
        this.setUrl(url);
    }

    public String getOperatinghours() {
        return operatinghours;
    }

    public void setOperatinghours(String operatinghours) {
        this.operatinghours = operatinghours;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString()
    {
        return super.toString() + ", " + operatinghours + ", " + url;
    }

}
